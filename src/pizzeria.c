#include <stdio.h>
#include <stdlib.h>
#include <pizzeria.h>

#define DEBUG

#define NO_THREAD

#include <macros.h>



#define ERR_PIZZERIA_ALREADY_CLOSED "Pizzeria already closed"
#define ERR_PIZZERIA_ALREADY_OPENED "Pizzeria already opened"
#define ERR_JOIN_THREAD_FAILED "Join thread failed"
#define ERR_CREATE_THREAD_FAILED "Create thread failed"
#define ERR_DESTROY_THREAD_FAILED "Destroy thread failed"
#define ERR_MUTEX_INIT_FAILED "Mutex init failed"
#define ERR_MUTEX_LOCK_FAILED "Mutex lock failed"
#define ERR_MUTEX_UN_LOCK_FAILED "Mutex unlock failed"


const char *menu[] = {
	    "Bacon" ,
	    "Bolognese" ,
	    "Broccoli" ,
	    "Buddy" ,
	    "California" ,
	    "Cipolla" ,
	    "Costolla" ,
	    "Hawaii" ,
	    "Margareta" ,
	    "Mozzarella" ,
	    "Palermo" ,
	    "Popey" ,
	    "Salami" ,
	    "Sicilia" ,
	    "Siena" ,
	    "Vegas" ,
	    "Vulcan"
	};

const int PIZZA_COUNTS=sizeof(menu)/sizeof(char *);

static struct {
	struct tailhead *headp; /* Tail queue head. */
	int product_order;
	volatile int pizzeria_opened;
	volatile int kitchen_opened;
	pthread_t producer;
	pthread_t consumer;
	pthread_mutex_t mutex;
	TAILQ_HEAD(tailhead, pizza_t)
	head;
} pizzeria;

const char **pizzeria_get_menu(){
	return menu;
}

void *pizzeria_bake(void *param) {
	deb("Enter pizzeria_bake(void *param)");

	while (pizzeria.kitchen_opened) {
		int i;
		for (i = 0; i < BAKE_CAPACITY; i++) {
			struct pizza_t *make;
			MALLOC(make, sizeof(struct pizza_t));
			make->order = ++pizzeria.product_order;
			make->pizza = rand() % PIZZA_COUNTS;

			printf("Baking pizza, order:%d, value:%s\n", make->order, menu[make->pizza]);
			deb("Baking pizza, order:%d, value:%s\n", make->order, menu[make->pizza]);

#ifndef NO_THREAD
			pthread_mutex_lock(&pizzeria.mutex);
#endif

			TAILQ_INSERT_TAIL(&pizzeria.head, make, entries);
#ifndef NO_THREAD
			pthread_mutex_unlock(&pizzeria.mutex);
#endif

		}
		sleep(1);
	}
	return NULL;
}

static void sale_a_pizza() {
	struct pizza_t *sale = pizzeria.head.tqh_first;
	printf("\t\t\tEat pizza, order:%d, name:%s\n", sale->order,
			menu[sale->pizza]);

#ifndef NO_THREAD
	pthread_mutex_lock(&pizzeria.mutex);
#endif
	TAILQ_REMOVE(&pizzeria.head, pizzeria.head.tqh_first, entries);
#ifndef NO_THREAD
	pthread_mutex_unlock(&pizzeria.mutex);
#endif

	FREE(sale);
}

void *pizzeria_consume(void *param) {
	while (pizzeria.kitchen_opened) {
		sleep(KITCHEN_OPEN_WAIT);
		int i = 0;
		while (!TAILQ_EMPTY(&pizzeria.head) && i < GUESTS_MAX) {
			sale_a_pizza();
			i++;
		}
	}
	printf("\nPizzeria will be closed, let's sale the remaining pizzas!\n");
	while (!TAILQ_EMPTY(&pizzeria.head))
		sale_a_pizza();

	return (NULL);
}

int pizzeria_open() {
	deb("Enter pizzeria_open()");

	if (pizzeria.pizzeria_opened) {
		warn(ERR_PIZZERIA_ALREADY_OPENED);
		return EXIT_FAILURE;
	}

	TAILQ_INIT(&pizzeria.head);
	pizzeria.product_order = 0;
	time_t t;
	time(&t);
	srand(t);

#ifndef NO_THREAD
	if (pthread_mutex_init(&pizzeria.mutex, NULL)) {
		err(ERR_MUTEX_INIT_FAILED);
		return EXIT_FAILURE;
	}
#endif
	pizzeria.kitchen_opened = 1;
	pizzeria.pizzeria_opened = 1;

#ifndef NO_THREAD
	if (pthread_create(&pizzeria.producer, NULL, pizzeria_bake, NULL)) {
		err(ERR_CREATE_THREAD_FAILED);
		return EXIT_FAILURE;
	}

	if (pthread_create(&pizzeria.consumer, NULL, pizzeria_consume, NULL)) {
		err(ERR_CREATE_THREAD_FAILED);
		return EXIT_FAILURE;
	}
#endif
	printf("\nNow the pizzeria is opened\n\n");
	return EXIT_SUCCESS;
}

int pizzeria_close_kitchen() {
	deb("Enter pizzeria_close_kitchen()");
	if (!pizzeria.pizzeria_opened) {
		err(ERR_PIZZERIA_ALREADY_CLOSED);
		return EXIT_FAILURE;
	}

	pizzeria.kitchen_opened = 0;

#ifndef NO_THREAD
	if (pthread_join(pizzeria.producer, NULL)) {
		err(ERR_JOIN_THREAD_FAILED);
		return EXIT_FAILURE;
	}
#endif
	printf("\nNow the kitchen is closed, no more pizzas baken\n\n");
	return EXIT_SUCCESS;
}

int pizzeria_close() {
	deb("Enter pizzeria_close()");

	if (!pizzeria.pizzeria_opened) {
		err(ERR_PIZZERIA_ALREADY_CLOSED);
		return EXIT_FAILURE;
	}

	pizzeria.pizzeria_opened = 0;

#ifndef NO_THREAD
	if (pthread_join(pizzeria.consumer, NULL)) {
		err(ERR_JOIN_THREAD_FAILED);
		return EXIT_FAILURE;
	}
#endif
	printf("\nNow the pizzeria is closed, no more pizzas remaining\n");
	printf("Are there remaining pizzas? %s\n",
			TAILQ_EMPTY(&pizzeria.head) ? "No" : "Yes");

	return EXIT_SUCCESS;
}

struct pizza_t *pizzeria_order(int index) {
	if(pizzeria.kitchen_opened!=1)
		return NULL;

	struct pizza_t *p=NULL;
	TAILQ_FOREACH(p, &pizzeria.head, entries) {
		if (p->pizza==index) {
#ifndef NO_THREAD
			pthread_mutex_lock(&pizzeria.mutex);
#endif
			TAILQ_REMOVE(&pizzeria.head, p, entries);
#ifndef NO_THREAD
			pthread_mutex_unlock(&pizzeria.mutex);
#endif
			return (p);
		}
	}
	return NULL;
}

struct pizza_t *pizzeria_wait_order(int index) {
	deb("Enter pizzeria_wait_order(int index:%d)", index);
	if (!pizzeria.pizzeria_opened) {
		warn(ERR_PIZZERIA_ALREADY_CLOSED);
		return NULL;
	}

	struct pizza_t *p=NULL;
	while(!p) {
		p=pizzeria_order(index);
	}
	return (p);
}


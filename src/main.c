
#include <CUnit/Basic.h>
#include <stdlib.h>
#include <syslog.h>
#include <pizzeria.h>

#define TEST

const int OPENING_TIME = 2; //sec
const int DISCOUNT_TIME = 2; // sev

void pizzeria() {
	pizzeria_open();
	sleep(OPENING_TIME);
	pizzeria_close_kitchen();
	sleep(DISCOUNT_TIME);
	pizzeria_close();
}


int init_test() {
	return 0;
}

int deinit_test() {
	return 0;
}

void open_test() {
	CU_ASSERT_EQUAL(EXIT_FAILURE, pizzeria_close_kitchen());

	CU_ASSERT_EQUAL(EXIT_FAILURE, pizzeria_close());
	CU_ASSERT_PTR_NULL(pizzeria_order(0));
	CU_ASSERT_PTR_NULL(pizzeria_wait_order(0));

	CU_ASSERT_EQUAL(EXIT_SUCCESS, pizzeria_open());

	CU_ASSERT_PTR_NOT_NULL(pizzeria_get_menu());
}

void close_test() {
	CU_ASSERT_EQUAL(EXIT_FAILURE, pizzeria_open());

	CU_ASSERT_EQUAL(EXIT_SUCCESS, pizzeria_close_kitchen());

	CU_ASSERT_EQUAL(EXIT_SUCCESS, pizzeria_close());

	CU_ASSERT_EQUAL(EXIT_FAILURE, pizzeria_close_kitchen());
	CU_ASSERT_EQUAL(EXIT_FAILURE, pizzeria_close());

}

void test() {
	CU_pSuite suite =NULL;
	if (CUE_SUCCESS!=CU_initialize_registry())
		return;

	suite = CU_add_suite("Suite 1", init_test, deinit_test);
	if (NULL==suite) {
		CU_cleanup_registry();
		goto exit;
	}

	if ( (NULL==CU_add_test(suite, "Open test", open_test)) ||
		 (NULL==CU_add_test(suite, "Close test", close_test)) ) {
		goto exit;
	}


	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
exit:
	CU_cleanup_registry();
}

int main(int argc, char **argv) {
	openlog("PIZZERIA", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL1);
	setlogmask(LOG_UPTO(LOG_DEBUG));

#ifdef TEST
	test();
#else
	pizzeria();
#endif
	closelog();
	return (EXIT_SUCCESS);
}



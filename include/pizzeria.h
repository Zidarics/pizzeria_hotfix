
#ifndef SRC_PIZZERIA_H_
#define SRC_PIZZERIA_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <sys/queue.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>


#define GUESTS_MAX 7

#define BAKE_CAPACITY 10

#define KITCHEN_OPEN_WAIT 2

struct pizza_t {
    int order;
    int pizza;
    TAILQ_ENTRY(pizza_t) entries;
};

int pizzeria_open();

int pizzeria_close_kitchen();

int pizzeria_close();


const char **pizzeria_get_menu();

struct pizza_t *pizzeria_order(int index);

struct pizza_t *pizzeria_wait_order(int index);

#endif /* SRC_PIZZERIA_H_ */

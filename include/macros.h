/*
 * macros.h
 *
 *  Created on: May 4, 2020
 *      Author: zamek
 */

#ifndef MACROS_H_
#define MACROS_H_

#include <syslog.h>
#include <stdarg.h>

#define MALLOC(ptr,size) \
	do { \
		ptr=malloc(size);	\
		if (!ptr)		\
			abort();	\
	} while(0)

#define FREE(ptr) \
	do {	\
		free(ptr);	\
		ptr=NULL;	\
	} while(0)


#ifdef DEBUG
#define deb(...) syslog(LOG_DEBUG, __VA_ARGS__)
#else
#define deb(...)
#endif

#define err(...) syslog(LOG_ERR, __VA_ARGS__)

#define warn(...) syslog(LOG_WARNING, __VA_ARGS__)

#define info(...) syslog(LOG_INFO, __VA_ARGS__)


#endif /* MACROS_H_ */
